import React, { useState, useEffect } from "react";

export default function HasilNo5() {
  const [dataAPI, setDataAPI] = useState([]);

  useEffect(() => {
    fetch(
      "https://cors-anywhere.herokuapp.com/https://api.eratani.co.id/getAboutFarmer",
      {
        method: "POST",
      }
    )
      .then((response) => response.json())
      .then((data) => {
        setDataAPI(data.data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }, []);

  return (
    <div>
      <ul>
        {dataAPI.map((item) => (
          <div>
            <li>{item.title}</li>
            <img src={item.image} alt="" style={{width:"200px"}}/>
            <div dangerouslySetInnerHTML={{ __html: item.description }} />
          </div>
        ))}
      </ul>
    </div>
  );
}
