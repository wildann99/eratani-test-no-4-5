import React, { useState } from "react";
import "./Body.css";
import data from "./utils/MOCK_DATA.json";

export default function Body() {
  const [dataAPI, setDataAPI] = useState(data);
  const [searchInput, setSearchInput] = useState("");
  const [filteredResults, setFilteredResults] = useState([]);

  const [addData, setaddData] = useState({
    id: "",
    first_name: "",
    last_name: "",
    email: "",
    gender: "",
  });

  const searchItems = (searchValue) => {
    setSearchInput(searchValue);
    if (searchInput !== "") {
      const filteredData = data.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(data);
    }
  };

  const handleAddFormChange = (event) => {
    event.preventDefault();
    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;
    const newFormData = { ...addData };
    newFormData[fieldName] = fieldValue;
    setaddData(newFormData);
  };

  const handleAddFormSubmit = (event) => {
    event.preventDefault();
    const newContact = {
      id: addData.id,
      first_name: addData.first_name,
      last_name: addData.last_name,
      email: addData.email,
      gender: addData.gender,
    };

    const newContacts = [...dataAPI, newContact];
    setDataAPI(newContacts);
  };

  const handleDeleteClick = (e) => {
    const newContacts = [...dataAPI];
    newContacts.splice(e.target.value, 1);
    setDataAPI(newContacts);
  };

  return (
    <div className="body">
      <input
        type="text"
        name="search"
        id=""
        placeholder="search"
        onChange={(e) => searchItems(e.target.value)}
        className="search"
      />

      <div className="add-form">
      <div>Tambah Data</div>
      <form onSubmit={handleAddFormSubmit}>
        <input
          type="number"
          name="id"
          required="required"
          placeholder="Enter a ID"
          onChange={handleAddFormChange}
        />

        <input
          type="text"
          name="first_name"
          required="required"
          placeholder="Enter a first_name..."
          onChange={handleAddFormChange}
        />
        <input
          type="text"
          name="last_name"
          required="required"
          placeholder="Enter an last_name..."
          onChange={handleAddFormChange}
        />
        <input
          type="email"
          name="email"
          required="required"
          placeholder="Enter a email..."
          onChange={handleAddFormChange}
        />
        <input
          type="text"
          name="gender"
          required="required"
          placeholder="Enter an gender..."
          onChange={handleAddFormChange}
        />

        <button type="submit">Add</button>
      </form>
      </div>

      <table>
        <thead>
          <th>ID</th>
          <th>first_name</th>
          <th>last_name</th>
          <th>email</th>
          <th>gender</th>
          <th>Action</th>
        </thead>

        {searchInput.length > 1 ? (
          <>
            {filteredResults.map((item, idx) => (
              <tbody key={item.id}>
                <td>{item.id}</td>
                <td>{item.first_name}</td>
                <td>{item.last_name}</td>
                <td>{item.email}</td>
                <td>{item.gender}</td>
                <td>
                  <button className="edit">Edit</button>
                  <button
                    value={item.idx}
                    onClick={handleDeleteClick}
                    className="delete"
                  >
                    Delete
                  </button>
                </td>
              </tbody>
            ))}
          </>
        ) : (
          <>
            {dataAPI.map((item, idx) => (
              <tbody key={item.id}>
                <td>{item.id}</td>
                <td>{item.first_name}</td>
                <td>{item.last_name}</td>
                <td>{item.email}</td>
                <td>{item.gender}</td>
                <td>
                  <button className="edit">Edit</button>
                  <button
                    value={idx}
                    onClick={handleDeleteClick}
                    className="delete"
                  >
                    Delete
                  </button>
                </td>
              </tbody>
            ))}
          </>
        )}
      </table>

    </div>
  );
}
